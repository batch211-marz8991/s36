//setup the dependencies/imports
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes")

//server setup

const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Database connection
//Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://admin123:admin123@project0.zprhgcb.mongodb.net/s36?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection
db.on('error',()=>console.error('Connection Error.'))
db.once('open',()=>console.log('Connected to MongoDB!'))

app.use("/tasks",taskRoutes);


app.listen(port, () => console.log(`Server running at port ${port}`));


